package version2_6;




import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class TankClient extends Frame{
    public static final int GAME_WIDTH = 1000;
    public static final int GAME_HEIGHT = 700;
    int x=50,y=50;
    Image offScreenImage = null;
    Blood b;

    int win;
    Tank myTank;
    //    Tank enemyTank = new Tank(100,100,false,this);
    Wall w = new Wall(250,250,250,10,this);

    List<Missile> missiles = new ArrayList<>();
    List<Explode> explodes = new ArrayList<>();
    List<Tank> tanks = new ArrayList<>();

    public void launchFrame() {
        b = new Blood();
        myTank = new Tank(x,y,true,this);
        for(int i = 0; i < 5; i++) {
            tanks.add(new Tank(50 + 40 * (i + 1), 50, false, this));
        }

        this.setLocation(300,100);
        this.setSize(GAME_WIDTH,GAME_HEIGHT);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        setResizable(false);
        this.setBackground(Color.GREEN);
        this.addKeyListener(new KeyMonitor());
        setVisible(true);
        new Thread(new PaintThread()).start();
    }
    public void paint(Graphics g) {
        g.drawString("missiles count:"+missiles.size(),10,50);
        g.drawString("explodes count:"+explodes.size(),10,70);
        g.drawString("tanks count: " + tanks.size(), 10, 90);
        g.drawString("blood count: " + myTank.getLife(), 10, 110);

        if(tanks.size() <= 0 && this.win!=1) {
            for (int i = 0; i < 5; i++) {
                tanks.add(new Tank(50 + 40 * (i + 1), 50, false,
                        this));
            }
            win=1;
        }
        for(int i = 0;i < missiles.size();i++)
        {
            Missile m = missiles.get(i);
            m.hitTanks(tanks);
            m.hitTank(myTank);

            m.hitWall(w);
            m.draw(g);
//            if(m.isLive()) missiles.remove(m);
//            else m.draw(g);
        }
        for (int i = 0; i < explodes.size(); i++) {
            Explode e = explodes.get(i);
            e.draw(g);
        }

        for (int i = 0; i < tanks.size(); i++) {
            tanks.get(i).collidesWithWall(w);
            tanks.get(i).collidesWithTanks(tanks);

            tanks.get(i).draw(g);
        }

        w.draw(g);
        myTank.draw(g);
        b.draw(g);
        myTank.eat(b);
        myTank.collidesWithWall(w);
        myTank.collidesWithTanks(tanks);



        if(tanks.size() <= 0) {
            g.setColor(Color.RED);
            g.setFont(new Font("Didot", Font.BOLD, 60));
            g.drawString("You Win!", 100, 300);

        }
        if (!myTank.isAlive()){
            g.setColor(Color.pink);
            g.setFont(new Font("Didot", Font.BOLD, 40));
            g.drawString("You failed, press F2 to start again.", 100, 300);
        }
    }
    public void update(Graphics g){
        if (offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
        }
        Graphics gOffScreen = offScreenImage.getGraphics();
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.GREEN);
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);
        print(gOffScreen);
        g.drawImage(offScreenImage, 0, 0, null);
    }
    private class KeyMonitor extends KeyAdapter{
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_F2) {
                restart();
            }
            myTank.kyePressed(e);
        }
        public void keyReleased(KeyEvent e) {
            myTank.keyReleased(e);
        }
    }
    public static void main(String[] args) {
        TankClient tc = new TankClient();
        tc.launchFrame();
    }
    private class PaintThread implements Runnable{
        public void run() {
            while(true) {
                repaint();
                try {
                    Thread.sleep(10);
                }catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void restart() {
        myTank.setAlive(false);
        myTank = new Tank(50, 50, true, this);
        tanks.clear();

        for (int i = 0; i < 5; i++) {
            tanks.add(new Tank(50 + 40 * (i + 1), 50, false, this));

        }
        missiles.clear();
        explodes.clear();
        b.setLive(false);
        b = new Blood();
    }
}