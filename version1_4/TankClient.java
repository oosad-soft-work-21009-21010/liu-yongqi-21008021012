package version1_4;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class TankClient extends Frame{
    public static final int GAME_WIDTH = 800;
    public static final int GAME_HEIGHT = 600;
    int x=50,y=50;
    Image offScreenImage = null;
    Tank myTank = new Tank(x,y,this);

    List<Missile> missiles = new ArrayList<>();
    public void launchFrame() {
        this.setLocation(300,100);
        this.setSize(GAME_WIDTH,GAME_HEIGHT);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        setResizable(false);
        this.setBackground(Color.GREEN);
        this.addKeyListener(new KeyMonitor());
        setVisible(true);
        new Thread(new PaintThread()).start();
    }
    public void paint(Graphics g) {
        g.drawString("missiles count:"+missiles.size(),10,50);
        for(int i = 0;i < missiles.size();i++)
        {
            Missile m = missiles.get(i);
            m.draw(g);
//            if(m.isLive()) missiles.remove(m);
//            else m.draw(g);
        }
        myTank.draw(g);
    }
    public void update(Graphics g){
        if (offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
        }
        Graphics gOffScreen = offScreenImage.getGraphics();
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.GREEN);
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);
        print(gOffScreen);
        g.drawImage(offScreenImage, 0, 0, null);
    }
    private class KeyMonitor extends KeyAdapter{
        public void keyPressed(KeyEvent e) {
            myTank.kyePressed(e);
        }
        public void keyReleased(KeyEvent e) {
            myTank.keyReleased(e);
        }
    }
    public static void main(String[] args) {
        TankClient tc=new TankClient();
        tc.launchFrame();
    }
    private class PaintThread implements Runnable{
        public void run() {
            while(true) {
                repaint();
                try {
                    Thread.sleep(100);
                }catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
