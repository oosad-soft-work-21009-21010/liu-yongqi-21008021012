package version1_4;

import java.awt.*;

public class Missile {
    public static final  int XSPEED = 10;
    public static final int YSPEED = 10;
    public static final int WIDTH = 10;
    public static final int HEIGHT = 10;
    private int x,y;
    Tank.Direction dir;
    private boolean live = true;

    TankClient tc;
    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public Missile(int x, int y, Tank.Direction dir,TankClient tankClient)
    {
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.tc = tankClient;
    }
    public void draw(Graphics g)
    {
        Color c = g.getColor();
        g.setColor(Color.BLACK);
        g.fillOval(x,y,10,10);
        g.setColor(c);
        move();
    }
    private void move()
    {
        switch (dir)
        {
            case L:
                x -= XSPEED;
                break;
            case LU:
                x -= XSPEED;
                y -= YSPEED;
                break;
            case U:
                y -= YSPEED;
                break;
            case RU:
                x += XSPEED;
                y -= YSPEED;
                break;
            case R:
                x += XSPEED;
                break;
            case RD:
                x += XSPEED;
                y += YSPEED;
                break;
            case D:
                y += YSPEED;
                break;
            case LD:
                x -= XSPEED;
                y += YSPEED;
                break;
        }
        if(x < 0 || y < 0 || x > TankClient.GAME_WIDTH ||
        y > TankClient.GAME_HEIGHT)
        {
            live = false;
            tc.missiles.remove(this);
        }
    }
}
