package version0_4;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class TankClient extends Frame {

    int x = 50, y = 50;

    public void launchFrame() {
        this.setLocation(300, 100);
        this.setSize(800, 600);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        setResizable(false);
        this.setBackground(Color.GREEN);
        setVisible(true);
        new Thread(new PaintThread()).start();
    }

    public void paint(Graphics g) {
        Color c = g.getColor();
        g.setColor(Color.red);
        g.fillOval(x, y, 30, 30);
        g.setColor(c);

        y += 5;
    }

    public static void main(String[] args) {
        TankClient tc = new TankClient();
        tc.launchFrame();

    }

    private class PaintThread implements Runnable {
        public void run() {
            while (true) {
                repaint();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}