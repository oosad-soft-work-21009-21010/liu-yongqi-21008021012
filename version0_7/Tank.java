package version0_7;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Tank {
    public static final  int XSPEED = 5;
    public static final int YSPEED = 5;
    private int x,y;
    public Tank(int x,int y)
    {
        this.x = x;
        this.y = y;
    }
    public void draw(Graphics g)
    {
        Color c = g.getColor();
        g.setColor(Color.RED);
        g.fillOval(x,y,30,30);
        g.setColor(c);
    }

    public void kyePressed(KeyEvent e)
    {
        int key = e.getKeyCode();
        switch (key) {
            case KeyEvent.VK_LEFT:
                x -= XSPEED;
                break;
            case KeyEvent.VK_UP:
                y -= YSPEED;
                break;
            case KeyEvent.VK_RIGHT:
                x += XSPEED;
                break;
            case KeyEvent.VK_DOWN:
                y += YSPEED;
                break;
        }
    }
}
